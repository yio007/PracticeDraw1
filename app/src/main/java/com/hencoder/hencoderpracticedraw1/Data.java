package com.hencoder.hencoderpracticedraw1;

/*
 *  @创建者:   Yio
 *  @创建时间:  2017/11/21 15:52
 *  @描述：    TODO
 */

public class Data {

    public Data(String name, float number, String color) {
        this.name = name;
        this.number = number;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public float getNumber() {
        return number;
    }

    public String getColor() {
        return color;
    }

    private String name;
    private float number;
    private String color;
}
