package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Practice8DrawArcView extends View {

    public Practice8DrawArcView(Context context) {
        super(context);
    }

    public Practice8DrawArcView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Practice8DrawArcView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
//        练习内容：使用 canvas.drawArc() 方法画弧形和扇形
        int left = canvas.getWidth() / 2 - 200;
        int top = canvas.getHeight() / 2 - 100;
        RectF oval = new RectF(left, top, left + 300, top + 200);
        float startAngle = -110;
        float sweepAngle = 80;
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        canvas.drawArc(oval, startAngle, sweepAngle, true, paint);

        startAngle = 30;
        sweepAngle = 120;
        canvas.drawArc(oval, startAngle, sweepAngle, false, paint);

        startAngle = -180;
        sweepAngle = 60;
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawArc(oval, startAngle, sweepAngle, false, paint);
    }
}
