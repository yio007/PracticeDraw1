package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.hencoder.hencoderpracticedraw1.Data;

import java.util.ArrayList;
import java.util.List;

public class Practice11PieChartView extends View {

    private List<Data> datas;
    private Paint paint;
    private RectF rectF;
    private float total;
    private float max;

    float startAngle = 0f; // 开始的角度
    float sweepAngle;      // 扫过的角度
    float lineAngle;       // 当前扇形一般的角度
    float radius = 200;
    float lineLength = 20;//白色直线的长度

    float lineStartX = 0f; // 直线开始的X坐标
    float lineStartY = 0f; // 直线开始的Y坐标
    float lineEndX;        // 直线结束的X坐标
    float lineEndY;        // 直线结束的Y坐标

    public Practice11PieChartView(Context context) {
        super(context);
        init();
    }

    public Practice11PieChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Practice11PieChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        datas = new ArrayList<>();
        Data data = new Data("Gingerbread", 50.0f, "#9C27B0");
        datas.add(data);
        data = new Data("Ice Cream Sandwich", 18.0f, "#9E9E9E");
        datas.add(data);
        data = new Data("Jelly Bean", 22.0f, "#009688");
        datas.add(data);
        data = new Data("KitKat", 27.0f, "#2397F3");
        datas.add(data);
        data = new Data("Lollipop", 40.0f, "#F44336");
        datas.add(data);
        data = new Data("Marshmallow", 20.0f, "#FFC107");
        datas.add(data);
        total = 0.0f;
        max = Float.MIN_VALUE;
        for (Data d : datas) {
            total += d.getNumber();
            max = Math.max(max, d.getNumber());
        }
        paint = new Paint();
        paint.setStrokeWidth(2);
        paint.setTextSize(30);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /*canvas.translate(canvas.getWidth() / 2, canvas.getHeight() / 2);  // 将画布(0，0)坐标点移到画布的中心
        for (Data data : datas) {
            if (isMove) {
                canvas.translate(-lineStartX * 0.1f, -lineStartY * 0.1f);
                isMove = false;
            }
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.parseColor(data.getColor()));
            sweepAngle = data.getNumber() / total * 360f;
            lineAngle = startAngle + sweepAngle / 2;
            lineStartX = 300 * (float) Math.cos(lineAngle / 180 * Math.PI);
            lineStartY = 300 * (float) Math.sin(lineAngle / 180 * Math.PI);
            lineEndX = 350 * (float) Math.cos(lineAngle / 180 * Math.PI);
            lineEndY = 350 * (float) Math.sin(lineAngle / 180 * Math.PI);
            if (data.getNumber() == max) {
                canvas.translate(lineStartX * 0.1f, lineStartY * 0.1f);
                canvas.drawArc(rectF, startAngle, sweepAngle, true, paint);
                isMove = true;
            } else {
                canvas.drawArc(rectF, startAngle, sweepAngle - 1.0f, true, paint);
            }
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawLine(lineStartX, lineStartY, lineEndX, lineEndY, paint);
            if (lineAngle > 90 && lineAngle <= 270) {
                canvas.drawLine(lineEndX, lineEndY, lineEndX - 50, lineEndY, paint);
                canvas.drawText(data.getName(), lineEndX - 50 - 10 - paint.measureText(data.getName()), lineEndY, paint);
            } else {
                canvas.drawLine(lineEndX, lineEndY, lineEndX + 50, lineEndY, paint);
                canvas.drawText(data.getName(), lineEndX + 50 + 10, lineEndY, paint);
            }
            startAngle += sweepAngle;
        }*/
        float left = canvas.getWidth() / 2 - radius;
        float top = canvas.getHeight() / 2 - radius;
        rectF = new RectF(left, top, left + radius * 2, top + radius * 2);

        lineStartX = canvas.getWidth() / 2;
        lineStartY = canvas.getHeight() / 2;
        for (Data data : datas) {

            sweepAngle = data.getNumber() / total * 360.0f;
            float angle = startAngle + sweepAngle / 2;
            lineStartX = (float) (left + radius + radius * Math.cos(angle * Math.PI / 180.0f));
            lineStartY = (float) (top + radius + radius * Math.sin(angle * Math.PI / 180.0f));
            lineEndX = (float) (left + radius + (radius + lineLength) * Math.cos(angle * Math.PI / 180.0f));
            lineEndY = (float) (top + radius + (radius + lineLength) * Math.sin(angle * Math.PI / 180.0f));
            //画扇形
            paint.setColor(Color.parseColor(data.getColor()));
            if (data.getNumber() == max) {
                canvas.save();
                //往外部移动画布
                canvas.translate((lineStartX - canvas.getWidth() / 2) * 0.1f, (lineStartY - canvas.getHeight() / 2) * 0.1f);
                canvas.drawArc(rectF, startAngle, sweepAngle, true, paint);
            } else {
                canvas.drawArc(rectF, startAngle, sweepAngle - 1, true, paint);
            }
            //画指示线
            paint.setColor(Color.WHITE);
            canvas.drawLine(lineStartX, lineStartY, lineEndX, lineEndY, paint);

            float textX;
            float stopX;
            if (angle < 90 || angle > 270) {
                stopX = lineEndX + lineLength * 3;
                textX = stopX + 20;
            } else {
                stopX = lineEndX - lineLength * 3;
                textX = stopX - 20 - paint.measureText(data.getName());
            }
            //继续画连着的第二个白色直线
            canvas.drawLine(lineEndX, lineEndY, stopX, lineEndY, paint);
            //画对应的文本信息
            canvas.drawText(data.getName(), textX, lineEndY, paint);

            startAngle += sweepAngle;
            canvas.restore();
        }
    }
}
