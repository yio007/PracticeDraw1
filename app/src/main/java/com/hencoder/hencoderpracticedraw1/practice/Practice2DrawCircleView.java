package com.hencoder.hencoderpracticedraw1.practice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class Practice2DrawCircleView extends View {

    private Paint paint;

    public Practice2DrawCircleView(Context context) {
        this(context, null);
    }

    public Practice2DrawCircleView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Practice2DrawCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //画实心黑色圆
        float cx = 300;
        float cy = 300;
        float radius = 150;
        canvas.drawCircle(cx, cy, radius, paint);

        //画实心蓝色圆
        cy = 800;
        paint.setColor(Color.parseColor("#4A90E2"));
        canvas.drawCircle(cx, cy, radius, paint);

        //画空心黑色圆
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.BLACK);
        cx = 800;
        cy = 300;
        canvas.drawCircle(cx, cy, radius, paint);
        //画空心黑色环
        paint.setStrokeWidth(20);
        cx = 800;
        cy = 800;
        canvas.drawCircle(cx, cy, radius, paint);
//        练习内容：使用 canvas.drawCircle() 方法画圆
//        一共四个圆：1.实心圆 2.空心圆 3.蓝色实心圆 4.线宽为 20 的空心圆
    }
}
